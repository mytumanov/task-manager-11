package ru.mtumanov.tm.controller;

import java.util.List;

import ru.mtumanov.tm.api.ITaskController;
import ru.mtumanov.tm.api.ITaskService;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        int index  = 1;
        final List<Task> tasks = taskService.findAll();
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName() + " : " + task.getDescription());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK BY ID]");  
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
       }  
        showTask(task);  
        System.out.println("[OK]");  
    }

    @Override
    public void showTasktByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");  
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }  
        showTask(task);  
        System.out.println("[OK]"); 
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");       
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");  
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");       
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]"); 
    }

    @Override
    public void updateTaskById() {
        System.out.println("[SHOW TASK BY ID]");       
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();  
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateById(id, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");       
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;  
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("ID: " + task.getId());
    }
    
}
