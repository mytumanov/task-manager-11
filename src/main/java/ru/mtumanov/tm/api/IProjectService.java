package ru.mtumanov.tm.api;

import ru.mtumanov.tm.model.Project;

public interface IProjectService extends IProjectRepository {
    
    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);
    
}
