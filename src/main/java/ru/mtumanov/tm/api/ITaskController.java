package ru.mtumanov.tm.api;

public interface ITaskController {
    
    void createTask();

    void showTasks();

    void clearTasks();

    void showTaskById();

    void showTasktByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

}
