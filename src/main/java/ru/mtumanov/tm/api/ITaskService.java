package ru.mtumanov.tm.api;

import ru.mtumanov.tm.model.Task;

public interface ITaskService extends ITaskRepository {
    
    Task create(String name, String description);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}
