package ru.mtumanov.tm.api;

public interface IProjectController {

    void createProject();

    void showProjectById();

    void showProjectByIndex();

    void showProjects();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();
    
}
